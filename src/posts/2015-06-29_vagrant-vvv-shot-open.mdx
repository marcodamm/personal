---
title: VVV or Varying Vagrant Vagrants
date: 2015-06-29
---

Creating your own Vagrantfile or forking an existing one allows you to set up a
highly customized virtual machine (VM) in a matter of minutes on as many
machines across various platforms. Those virtual machines can then be used as
part of your local development environment. With Vagrant you can simply define
all parameters for your virtual machine in the so called Vagrantfile. After
that you simply run `vagrant up` and watch the magic happen.

I set up my local development environment with Vagrant about one month ago.
Getting everything up and running wasn't too hard, and deploying such
environments on various machines is super easy and a real timesaver. Because we
are a small company, and because we also partly rely on our external service
partners for development, there is no urgent need for multiple fully fledged
onsite dev-environments right now. Nontheless, there are times when you might
want to share your dev-machine's guest OS (aka the vagrant VM), across multiple
devices to collaborate with the designers, external partners or maybe even your
customers.

Luckily, Vagrant has a system in place which allows you to not only share your
VM as an encapsulated guest on with your own machine, but also a really neat
option to allow sharing your VM with devices (smartphones, tablets, desktop
computers, you-name-it) on your local network.

As our products are all wordpress based solutions, I choose a combination of
[VVV](https://github.com/Varying-Vagrant-Vagrants/VVV) and
[VVV-Multisite](https://github.com/Varying-Vagrant-Vagrants/vvv-multisite) for
provisioning my initial VM setup. After cloning the VVV repository and after
running the initial `vagrant up` I had the following virtual machine setup
ready to go:

1. [Ubuntu](http://www.ubuntu.com/) 14.04 LTS (Trusty Tahr)
1. [WordPress Develop](https://develop.svn.wordpress.org/trunk/)
1. [WordPress Stable](https://wordpress.org/)
1. [WordPress Trunk](https://core.svn.wordpress.org/trunk/)
1. [WordPress Sudbirectory Mutisite Trunk](https://github.com/Varying-Vagrant-Vagrants/vvv-multisite)
1. [WordPress Sudbirectory Mutisite Stable](https://github.com/Varying-Vagrant-Vagrants/vvv-multisite)
1. [WordPress Subdomain Mutisite Trunk](https://github.com/Varying-Vagrant-Vagrants/vvv-multisite)
1. [WordPress Subdomain Mutisite Stable](https://github.com/Varying-Vagrant-Vagrants/vvv-multisite)
1. [WP-CLI](http://wp-cli.org/)
1. [nginx](http://nginx.org/) 1.6.x
1. [mysql](https://www.mysql.com/) 5.5.x
1. [php-fpm](http://php-fpm.org/) 5.5.x
1. [memcached](http://memcached.org/) 1.4.13
1. PHP [memcache extension](https://pecl.php.net/package/memcache/3.0.8/) 3.0.8
1. PHP [xdebug extension](https://pecl.php.net/package/xdebug/2.2.5/) 2.2.5
1. PHP [imagick extension](https://pecl.php.net/package/imagick/3.1.2/) 3.1.2
1. [PHPUnit](https://phpunit.de/) 4.0.x
1. [ack-grep](http://beyondgrep.com/) 2.04
1. [git](http://git-scm.com/) 1.9.x
1. [subversion](https://subversion.apache.org/) 1.8.x
1. [ngrep](http://ngrep.sourceforge.net/usage.html)
1. [dos2unix](http://dos2unix.sourceforge.net/)
1. [Composer](https://github.com/composer/composer)
1. [phpMemcachedAdmin](https://code.google.com/p/phpmemcacheadmin/)
1. [phpMyAdmin](http://www.phpmyadmin.net/) (multi-language)
1. [Opcache Status](https://github.com/rlerdorf/opcache-status)
1. [Webgrind](https://github.com/jokkedk/webgrind)
1. [NodeJs](https://nodejs.org/) Current Stable Version
1. [grunt-cli](https://github.com/gruntjs/grunt-cli) Current Stable Version

I can now start this VM setup with another `vagrant up` and use `vagrant ssh`
to log into the VM from my CLI. The whole process was very smooth, and except
for manually creating a few tables to be populated by our heavily customized
theme I encountered no problems up until this point. Good!

Now, as I mentioned earlier we have currently no need to replicate this process
on a number of machines, but it's good to know that we could if we ever need
to. Besides allowing for super easy dev-environment roll-outs, Vagrant also has
a very nice sharing mechanism which in theory allows you to switch between a VM
only available to your host-OS to a VM available publicly for access across
various devices. Why would you share a VM across a (local) network? Well, you
could:

- **Cross-device-testing:** Test the current development status across various devices!
- **Collaboration:** Share the development status with your co-workers and have them access your VM for co-development

In theory switching from the standard `private_network` setup to a
`public_network` setup is as easy as changing the following lines in VVV's
Vagrantfile.

remove or comment out `# config.vm.network "private_network", ip:
"xxx.xxx.xxx.xxx"` add or comment in `config.vm.network "public_network"`

By removing the private_network part and adding the public_network option,
vagrant will try to configure your virtual machine to be available not it's
host OS (i.e. the machine running vagrant) but also to the local network. To
make these settings work you first have to restart your Vagrant VM by running:

`vagrant halt` and then `vagrant up`

During the startup process vagrant might ask you to choose a network interface
for bridging. This might look something like this:

	==> default: Available bridged network interfaces:
	1) wlan0
	2) eth0
	==> default: When choosing an interface, it is usually the one that is
	==> default: being used to connect to the internet.

Choose the appropriate network interface and let the startup process continue.
If everything boots up successfully, your VM will now be available across all
devices in the same network as the Vagrant VM.

I ran into one problem with this setup. I could not access the VM from other
devices via the .dev domains provided by the vvv-hosts files. This of course
makes sense because vagrant can only alter the hosts file on the machine it is
running on. For accessing your Vagrant VM from any other devices, you have a
few options:

## 1. Edit the accessing client's hosts file

If you only need to access your Vagrant VM from another single machine and this
machine happens to allow you to edit it's hosts file then this is arguably the
least disruptive and fastest solution. Editing the hosts file is usually only
possible on machines with traditional operating systems. That means you will be
able to edit a hosts file on a Windows, Linux or Mac OS X machine but probably
not on your smartphone. I have been able to edit my rooted Android smartphone's
hosts file successfully but I have not found a non-jailbreak solution for iOS
yet.

## 2. Edit your local network router's routing table

The most comprehensive but possibly disruptive solution for making a Vagrant VM
available across all devices in a network is to edit the network-providing
router's routing table (or hosts file). You will find this option on almost all
professional routers and on the more expensive consumer models.

## 3. Use a proxy software
If cannot access the client's hosts file (#1) and cannot configure your local
network's router to resolve domains to a local IP (#2) the your best bet is to
run a HTTP proxy server on the computer where Vagrant is running, and configure
your other devices to use the proxy.

[egalo has a fairly comprehensive tutorial on how to set up squid + squidman on Mac OS X and also some links for Windows HTTP Proxy software.](http://egalo.com/2012/05/29/testing-mac-web-site-using-local-hostname-on-mobile-device/)
